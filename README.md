# Dotfiles

## i3
### Dependencies:
- i3
- i3blocks
- i3lock
- font awesome
- consolekit
- dbus
- polkit-pkla-compat (on non-systemd systems)

### Optional Dependencies:
- feh (desktop background image)
- pasystray (PulseAudio / Volume systray app)
- NetworkManager and nm-applet (Network Manager systray app)
- lightsOn.sh (keep screensaver off when certain apps are in fullscreen mode)
- copyq (clipboard systray app)
- redshift-gtk (dim and redshift screen lights in the evening=
- pcmanfm (lightweight file manager)
- conky (desktop background system information)
- rofi (dmenu replacement and multi-launcher)
- keepassxc

## rxvt-unicode with Solarizr colors
### Dependencies:
- rxvt-unicode (compiled with: blink fading-colors font-styles iso14755 mousewheel perl startup-notification unicode3 xft)
- font: DejaVu Sans Mono
- font: Powerline Fonts (from: https://github.com/powerline/fonts)

## zsh
### Dependencies:
- oh-my-zsh (integrated via a git submodule!)

