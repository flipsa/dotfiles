!-------------------------------------------------------------------------------
! Xft settings
!-------------------------------------------------------------------------------

!Xft.dpi:                    96
!Xft.antialias:              true
!Xft.rgba:                   rgb
!Xft.hinting:                true
!Xft.hintstyle:              hintsfull

!-------------------------------------------------------------------------------
! URxvt settings
! Colours lifted from Solarized (http://ethanschoonover.com/solarized)
! More info at:
! http://pod.tst.eu/http://cvs.schmorp.de/rxvt-unicode/doc/rxvt.1.pod
!-------------------------------------------------------------------------------

URxvt.depth:                32
URxvt.geometry:             90x30
URxvt.transparent:          true
URxvt.fading:               25%
URxvt.urgentOnBell:         true
URxvt.visualBell:           true
URxvt.loginShell:           true
URxvt.saveLines:            5000
URxvt.internalBorder:       3
URxvt.lineSpace:            0
URxvt.inheritPixmap: False
URxvt.urlLauncher: google-chrome



! Fonts
URxvt.allow_bold:           false
/* URxvt.font:                 -*-terminus-medium-r-normal-*-12-120-72-72-c-60-iso8859-1 */
URxvt*font: xft:DejaVu Sans Mono:antialias=true:pixelsize=14,\
xft:unifont:size=11,\
xft:Inconsolata\ for\ Powerline:size=11,\
xft:FontAwesome:size=11,\
xft:fontawesome-webfont:size=11

URxvt*boldFont: xft:DejaVu Sans Mono:style=bold:antialias=true:pixelsize=14,\
xft:Inconsolata\ for\ Powerline:size=11,\
xft:FontAwesome:size=11,\
xft:fontawesome-webfont:size=11

! Fix font space
!URxvt*letterSpace: -1

! Scrollbar
URxvt.scrollStyle:          plain
URxvt.scrollBar:            false

! do not scroll with output; scroll in relation to buffer (with mouse scroll or Shift+Page Up); scroll back to the bottom on keypress
URxvt*scrollTtyOutput: false
URxvt*scrollWithBuffer: true
URxvt*scrollTtyKeypress: true

! Perl extensions
URxvt.perl-ext-common:      default,matcher,tabbed
URxvt.matcher.button:       1

! Cursor
URxvt.cursorBlink:          true
URxvt.cursorColor:          #657b83
URxvt.cursorUnderline:      false

! Pointer
URxvt.pointerBlank:         true

!!Source http://github.com/altercation/solarized

*background: #002b36
*foreground: #657b83
*fading: 40
*fadeColor: #002b36
*cursorColor: #93a1a1
*pointerColorBackground: #586e75
*pointerColorForeground: #93a1a1

!! black dark/light
*color0: #073642
*color8: #002b36

!! red dark/light
*color1: #dc322f
*color9: #cb4b16

!! green dark/light
*color2: #859900
*color10: #586e75

!! yellow dark/light
*color3: #b58900
*color11: #657b83

!! blue dark/light
*color4: #268bd2
*color12: #839496

!! magenta dark/light
*color5: #d33682
*color13: #6c71c4

!! cyan dark/light
*color6: #2aa198
*color14: #93a1a1

!! white dark/light
*color7: #eee8d5
*color15: #fdf6e3
